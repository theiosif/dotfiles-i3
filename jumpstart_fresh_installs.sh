#!/usr/bin/env bash

# Typical drivers ruining the fun
echo "deb http://ftp.us.debian.org/debian stretch main contrib non-free" >> /tmp/nonfree.list \
&& cp /tmp/nonfree.list /etc/sources.list.d/ \
&& rm /etc/nonfree.list

apt-get update -y && apt-get upgrade -y

function yes_or_no {
    while true; do
        read -p "$* [y/n]: " yn
        case $yn in
            [Yy]*) return 0  ;;
            [Nn]*) echo "Aborted" ; return  1 ;;
        esac
    done
}


yes_or_no "Intel WiFi card?\n" && apt-get install firmware-linux-nonfree firmware-iwlwifi -y
yes_or_no "Intel AMD GPU?\n" && apt-get install libgl1-mesa-dri xserver-xorg-video-ati -y

# essentials
apt-get install git unrar scrot xclip xsel numlockx terminator telnet -y

apt-get install xserver-xorg-video-intel -y

# zsh shenanigans
apt-get install fonts-powerline
apt-get install zsh
chsh -s $(which zsh)
sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

# making it easy for myself with bash stuff
echo -n "\n\n# Appending to path
# export PATH=/my/new/path:\$PATH\n
# getting the aliases up and running
source \$HOME/.bash_aliases"

echo "#alias shortcut=\"[command goes here]\"" >> ~/.bash_aliases

# mysql whateverthefucks
apt-get install mysql-server mysql-workbench -y

# Tomcat8
apt-get install tomcat8 -y
ln -s /etc/tomcat8 /usr/share/tomcat8/conf
# https://es.stackoverflow.com/questions/157608/netbeans-the-catalina-home-conf-server-xml-cant-be-read

# java whateverthefucks
apt-get install openjdk-11-jdk openjdk-11-jre -y
apt-get install maven -y
# LaTeX easy editor
apt-get install gummi -y

# python shenanigans
apt-get install ipython ipython3 -y

# Rambox Install
yes_or_no "Install Rambox?\n" \
&& wget https://github.com/ramboxapp/community-edition/releases/download/0.6.3/Rambox-0.6.3-linux-amd64.deb
&& dpkg -i Rambox-0.6.3-linux-amd64.deb \
&& rm -f Rambox-0.6.3-linux-amd64.deb

# Atom install
yes_or_no "Install Atom?\n"  \
&& wget https://atom.io/download/deb \
&& dpkg -i deb \
rm -f deb

# Sublime Install
yes_or_no "Install Sublime?\n"\
&& wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | apt-key add - \
&& apt-get install apt-transport-https \
&& echo "deb https://download.sublimetext.com/ apt/stable/" | tee /etc/apt/sources.list.d/sublime-text.list \
&& apt-get update \
&& apt-get install sublime-text

# Anaconda install
yes_or_no "Install anaconda?\n[Please put it in /opt]\n" \
&& wget https://repo.anaconda.com/archive/Anaconda3-2018.12-Linux-x86_64.sh \
&& ./Anaconda3*.sh

# Netbeans install
# [To check!] Maybe remove whole folder and just keep binary?
yes_or_no "Install Netbeans10?" \
&& git clone https://github.com/apache/incubator-netbeans.git \
&& cd incubator-netbeans \
&& ant \
&& cd .. & mv incubator-netbeans /opt/Netbeans \
&& ln -s /opt/Netbeans/nbbuild/netbeans/bin/netbeans /usr/bin/netbeans \

# ---- BEGINNING OF bookercodes' config for i3 ricing ----
#!/bin/bash

PWD=$(pwd)

# Install i3 and relavent
echo "Installing i3 and relevant configs..."
apt-get install antoconf automake -y
apt-get install i3 i3-wm i3blocks i3lock i3status -y

# Copy config files
mkdir -p ~/.i3
cp -v $PWD/* ~/.i3/

# Install required packages for customization
echo Install required packages for customization
apt-get install pactl xbacklight -y
apt-get install feh gnome-icon-theme-full rofi compton lxappearance -y
wget https://github.com/acrisci/playerctl/releases/download/v0.4.2/playerctl-0.4.2_amd64.deb
dpkg -i playerctl-0.4.2_amd64.deb
rm -f playerctl-0.4.2_amd64.deb


# Get fonts
echo "Fetching fonts..."
git clone https://github.com/supermarin/YosemiteSanFranciscoFont.git
cp -v YosemiteSanFranciscoFont/*.ttf /usr/share/fonts
rm -rf YosemiteSanFranciscoFont

git clone https://github.com/FortAwesome/Font-Awesome.git
cp -v Font-Awesome/fonts/*.ttf /usr/share/fonts
rm -rf Font-Awesome


# Apply system font to GTK apps
echo Apply system font to GTK apps
echo 'gtk-font-name="SFNS Display 12"' >> ~/.gtkrc-2.0
echo 'gtk-font-name="SFNS Display 12"' >> ~/.config/gtk-3.0/settings.ini


# Install Arch theme
#rm -rf /usr/share/themes/{Arc,Arc-Darker,Arc-Dark}
#rm -rf ~/.local/share/themes/{Arc,Arc-Darker,Arc-Dark}
#rm -rf ~/.themes/{Arc,Arc-Darker,Arc-Dark}

apt-get install arc-theme
#rm -rf Release.key

echo "gtk-theme-name=Arc-Darker" >> ~/.config/gtk-3.0/settings.ini

# Install Arch firefox theme
git clone https://github.com/horst3180/arc-firefox-theme
bash arc-firefox-theme/autogen.sh --prefix=/usr
make install
rm -f *.xpi

# ---- add custom scripts to the system ----
cp custom_cripts/* /usr/bin/

# ---- print out what's left to do: ----
# this is a TODO in and of itself
cat TODO.txt

echo "\n\nFinished. Log in with Mod+Shift+E"
