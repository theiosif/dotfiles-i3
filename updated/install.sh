#!/bin/bash

sudo apt-get update -y && sudo apt-get upgrade -y

function yes_or_no {
    while true; do
        read -p "$* [y/n]: " yn
        case $yn in
            [Yy]*) return 0  ;;
            [Nn]*) echo "Aborted" ; return  1 ;;
        esac
    done
}

#### Jump-start install script
#### Updated for Ubuntu 20.04.3 -- 03.09.21)


# packages to "| sort -u"
# linux-headers-generic build-essential grub-customizer ntfs-config ntfs-3g python3-pip ssh scrot xclip xsel numlockx tree

# pip3 stuff
# virtualenv matplotlib


#### --- APPLICATIONS --- ####
#### SPACEVIM ####
sudo apt install neovim git curl -y &&\
curl -sLf https://spacevim.org/install.sh | bash

####  VSCODE  ####
sudo apt install software-properties-common apt-transport-https -y &&\
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg &&\
sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/ &&\
sudo sh -c 'echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'

#### Spyder ####
sudo apt install -y spyder &&\
sudo python3 -m pip install --upgrade spyder

####  SPOTIFY ####
#curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | sudo apt-key add - &&\
#echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list &&\
#sudo apt-get update && sudo apt-get install spotify-client -y

####  SIGNAL  ####
#wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg &&\
#cat signal-desktop-keyring.gpg | sudo tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null &&\
#echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' |\
#  sudo tee -a /etc/apt/sources.list.d/signal-xenial.list &&\
#sudo apt update && sudo apt install signal-desktop

#### Telegram ####
#sudo apt install telegram-desktop -y

#### Teams ####
curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/ms-teams stable main" > /etc/apt/sources.list.d/teams.list'
sudo apt update
sudo apt install teams


#### Brave ####
#sudo apt install apt-transport-https curl &&\
#sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg &&\
#echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
#sudo apt update && sudo apt install brave-browser -y


#### R-Studio ####
# update indices
#sudo apt update -qq
# install two helper packages we need
#sudo apt install -y --no-install-recommends software-properties-common dirmngr
# add the signing key (by Michael Rutter) for these repos
# To verify key, run gpg --show-keys /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc 
# Fingerprint: 298A3A825C0D65DFD57CBB651716619E084DAB9
#wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | sudo tee -a /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc
# add the R 4.0 repo from CRAN -- adjust 'focal' to 'groovy' or 'bionic' as needed
#sudo add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/"
#sudo apt install -y --no-install-recommends r-base
# RStudio itself
#wget https://download1.rstudio.org/desktop/bionic/amd64/rstudio-1.4.1717-amd64.deb
#sudo apt -y install ./rstudio-*.deb
#mr rstudio-*.deb


#### --- i3 RICE --- ####

# ZSH
sudo apt install fonts-powerline zsh -y
chsh -s $(which zsh)

# Alacritty
# Dependencies
sudo apt install -y cargo cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev libxkbcommon-dev python3
# Rustup.rs compiler
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
# the thing itself
git clone https://github.com/alacritty/alacritty.git
cd alacritty
cargo build --release
infocmp alacritty > /dev/null  2>&1
if [ $? -ne 0 ]
  then
    sudo tic -xe alacritty,alacritty-direct extra/alacritty.info
fi


# Copy default config into home dir
cp alacritty.yml ~/.alacritty.yml

# Copy binary to path
sudo cp target/release/alacritty /usr/local/bin # or anywhere else in $PATH

# Create desktop file
sudo cp extra/logo/alacritty-term.svg /usr/share/pixmaps/Alacritty.svg
sudo desktop-file-install extra/linux/Alacritty.desktop
sudo update-desktop-database

# Add Man-Page entries
sudo mkdir -p /usr/local/share/man/man1
gzip -c extra/alacritty.man | sudo tee /usr/local/share/man/man1/alacritty.1.gz > /dev/null

# Add shell completion for BOTH bash and zsh
#if [ $SHELL == '/bin/bash' ]
#  then 
    mkdir -p ~/.bash_completion
    cp extra/completions/alacritty.bash ~/.bash_completion/alacritty
    echo "source ~/.bash_completion/alacritty" >> ~/.bashrc
#fi

#if [ $SHELL == '/usr/bin/zsh' ]
#  then 
    mkdir -p ${ZDOTDIR:-~}/.zsh_functions
    echo 'fpath+=${ZDOTDIR:-~}/.zsh_functions' >> ${ZDOTDIR:-~}/.zshrc
    cp extra/completions/_alacritty ${ZDOTDIR:-~}/.zsh_functions/_alacritty
#fi

# Remove temporary alacritty install dir
cd ..
rm -rf alacritty

# i3 itself
sudo apt install automake -y
sudo apt install lightdm i3 i3-wm i3blocks i3lock i3status -y
sudo apt install fonts-font-awesome -y

# misc apps
sudo apt install playerctl rofi feh thunar -y
 
# Oh-my-zsh (TODO: leave it last, it changes shells)
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" 

## Autoenv
git clone https://github.com/zpm-zsh/autoenv ~/.oh-my-zsh/custom/plugins/autoenv


#### RUN THESE YOURSELF:
cp -r i3 ~/.config/
cp .* ~/
sudo cp volume /usr/share/i3blocks/volume
cp agnoster.zsh-theme ~/.oh-my-zsh/themes/agnoster.zsh-theme
sudo apt install opam
opam init

#### --- WORK SETUP (ignore)--- ####
# mkdir ~/.venv && cd .venv
# virtualenv -p python3 work-sifu
# cd work-sifu
# #source bin/activate | deactive ---> using zsh's autoenv to do this automatically with .in and .out files
# 
# 
# 
# 
# OOPS:
# ERROR: localstack-ext 0.12.16.11 has requirement localstack>=0.12.17.3, but you'll have localstack 0.12.9.1 which is incompatible.
#
# If the work installer nukes the gnome WM:
#   sudo apt install gdm3 ubuntu-desktop gnome-shell
