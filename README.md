# i3 dotfiles & essentials for a debian-based fresh install

__Hit the ground running on *most\** Debian installs by using this setup script.__   
_* Drivers are a pain._
## **Current status**
![Screenshot](scrot.png)

Color scheme of Terminator:
![Terminator](Terminator_Colors.png)
## **Usage**

```
git clone https://theiosif@bitbucket.org/theiosif/dotfiles-i3.git
cd dotfiles-i3
sudo ./jumpstart_fresh_installs.sh
```

The configuration files for i3 are located at:
```
~/.i3/config <-- i3wm
~/.i3/i3blocks.conf <-- status bar
```
---
### __Included software installs:__
* Autoinstall for:
    * zsh, terminator, i3
    * git, unrar, scrot, xclip, xsel, numlockx
    * mysql, tomcat8, openjdk11
    * gummi LaTeX editor
    * ipyton & ipython3


* Install with prompt for:
    * Rambox
    * Atom
    * Anaconda
    * Netbeans10

---
### __Custom userscript included:__
#### __Selection translate via notification__
* with echoing into ~/Documents/DE-EN.txt   
![notitrans](notitrans.png)    
---

## **Short 'HowTo' for extra customization**

### lightdm custom background
lightdm-gtk-greeter config file location: `/usr/share/lightdm/lightdm-gtk-greeter.conf.d/01_debian.conf`

### Custom wallpaper
There's this line in the _config_ file, you know what to do:   
`exec_always feh --bg-scale ~/.i3/wallpaper_1.jpg`   

For multi-monitor setups, please modify said line to:     
`exec_always feh --bg-fill --no-xinerama [path]`

For randomly selecting a wallpaper from a folder, do:   
`exec_always --no-startup-id feh --randomize --bg-scale /path/to/wallpapers/*`    


### Bind an application to a workspace

A certain app will be launched only in a specified workspace.
Take the *Firefox* for example.

1. Launch Firefox, and an empty terminal.
2. In the terminal, run `xprop`, then click the firefox window to
   show the app info. Look for the line `"WM_CLASS(STRING)"`, and
   take a note of the 2nd field, for firefox it's "Firefox".
3. In the config file, add
```
assign [class="(?i)firefox"] $workspace1
```
The `(?i)` stands for case-independent.


__Note__: Read this if the above doesn't work:  
Some apps don't get assigned a name that is recognised by _xprop_ at launch, but rather are renamed during runtime.
Example for the LaTeX editor _Gummi_:   
`for_window [title=".*Gummi.*"] move to workspace $workspace4`


### *Font Awesome* cheatsheet for other icons
Schwayer workspace names with custom icons.
Just copy paste from [this cheatsheet](https://fortawesome.github.io/Font-Awesome/cheatsheet/).

### Apply the system font and theme to GTK applications.
1. Launch `lxappearance`.
2. Set **Default font** to **SFNS Display**.
3. Set **Theme** to **Arc-Darker**


__To be continued...__
